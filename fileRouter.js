const express = require("express");
const router = express.Router();
const fileController = require("./fileController");
const validateFile = require("./validateFile");

router.route("/").post(validateFile, fileController.createFile);
router.get("/", fileController.getFiles);
router.get("/:filename", fileController.getFileByName);
router.put("/:filename", fileController.updateFile);
router.delete("/:filename", fileController.deleteFile);

module.exports = router;
