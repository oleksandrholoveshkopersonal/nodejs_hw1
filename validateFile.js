const fs = require("fs").promises;
const fsSync = require("fs");
const path = require("path");

module.exports = validateFile = async (req, res, next) => {
  const { filename, content } = req.body;

  if (filename === undefined) {
    return res
      .status(400)
      .json({ message: "Please specify filename parameter" });
  }

  if (content === undefined) {
    return res
      .status(400)
      .json({ message: "Please specify content parameter" });
  }

  const fileExt = path.extname(filename);
  const correctExt = /^.*\.(log|txt|json|yaml|xml|js)$/gi.test(fileExt);

  if (!fsSync.existsSync("./files")) {
    try {
      await fs.mkdir(path.join(__dirname, "files"));
    } catch {
      console.log("Failed to create directory");
    }
  }

  if (fsSync.existsSync(`./files/${filename}`)) {
    return res.status(400).json({ message: "File already exists" });
  }

  if (!filename || !correctExt) {
    return res.status(400).json({ message: "Wrong file type" });
  }

  next();
};
